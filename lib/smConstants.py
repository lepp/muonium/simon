import enum
import os
import socket

import matplotlib.pyplot as plt

domain = "psi.ch"
default_wdb = "wd045"
default_wdb_adr = f"{default_wdb}.{domain}"
hostname = f"{socket.gethostname()}.{domain}"
path = os.environ["SIMONSYS"]
pcb_cfg_dir = os.path.join(path, "config/pcb")
scan_cfg_dir = os.path.join(path, "config/scan")
wdb_cfg_dir = os.path.join(path, "config/wdb")
template_dir = os.path.join(path, "templates")
www_dir = os.path.join(path, "www")
www_port = 8008
www_url = f"http://{hostname}:{www_port}"
www_cmd = ("python", "-m", "http.server", str(www_port), "--directory", www_dir)
simon_port = 8800
simon_url = f"http://{hostname}:{simon_port}"
sm_wdb_exe = "smWDB"
sm_wdb_demo_exe = "smWDBDemo.py"
wds_exe = "wds"
wds_port = 8080
wds_url = f"http://{hostname}:{wds_port}"
file_server_exe = "servefile.py"
file_server_port = 8880
file_server_url = f"http://{hostname}:{file_server_port}"
fig_inches = (20, 20)
fig_dpi = 100
x_lim = (-75, 75)
y_lim = (-75, 75)
x_label = "SiPM position [mm]"
y_label = "PCB position [mm]"
z_label = "Rate [Hz/mm^2]"
pill_cmap = plt.cm.plasma
cont_cmap = plt.cm.viridis_r
cont_linewidths = 0.5
plt_style = "dark_background"
sg_theme = "Black"
sg_font = ("Courier", 15)
win = None
win_timeout_ms = 1
demo_params = (1000000, 5, 0, 5, 30)
n_channels = 16
masc_port_dir = "/dev/serial/by-id"
masc_rs485_adr = 1
masc_timeout_s = 0.1
masc_rcv_trials = 3
masc_eg = 20000
masc_rev_per_mm = 0.8
masc_range_mm = (-70, 70)
masc_home_pos_mm = masc_range_mm[0]
masc_init_limit_pos_mm = -118.5
masc_init_limit_alarm = 2
masc_init_js_slow = 1
masc_init_js_fast = 10
masc_init_di = -1
masc_init_backup_distance_mm = 10


class ScanState(enum.IntEnum):
    START = -1
    IDLE = -2
    STOP = -3
