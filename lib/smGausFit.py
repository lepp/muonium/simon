# Mostly copied from https://scipy-cookbook.readthedocs.io/items/FittingData.html


import numpy as np
from scipy import optimize


def dist_2d(norm, u_x, u_y, s_x, s_y):
    """Returns a 2D Gaussian function with the given parameters"""
    s_x = float(s_x)
    s_y = float(s_y)

    return lambda x, y: norm * np.exp(-(((u_x - x) / s_x) ** 2 + ((u_y - y) / s_y) ** 2) / 2.) / (
            2. * np.pi * s_x * s_y)


def mom_2d(x, y, data):
    """Returns (norm, u_x, u_y, s_x, s_y) the Gaussian parameters of a 2D distribution by calculating its moments"""
    norm = data.sum()
    u_x = (x * data).sum() / norm
    u_y = (y * data).sum() / norm
    cov = np.cov(x, y, fweights=data.astype(np.uint64))
    s_x = np.sqrt(cov[0, 0])
    s_y = np.sqrt(cov[1, 1])

    return norm, u_x, u_y, s_x, s_y


def fit_2d(x, y, data, verbose=0):
    """Returns (norm, u_x, u_y, s_x, s_y) the Gaussian parameters of a 2D distribution found by a least squares fit"""
    params_init = mom_2d(x, y, data)
    if verbose:
        print("Initial parameters:")
        print(params_init)

    # Ignore PEP recommendation by IDE! This needs to be a lambda.
    errorfunction = lambda p: dist_2d(*p)(x, y) - data

    params_fit, status_fit = optimize.leastsq(errorfunction, params_init)
    # sigma can converge to negative solution, so take the modulus here
    params_fit[3] = np.abs(params_fit[3])
    params_fit[4] = np.abs(params_fit[4])
    if verbose:
        print("Fit parameters:")
        print(params_fit)

    return params_fit
