import numpy as np
import time

import smConstants as sc
import smGausFit as fit


def projection(pos, rate):
    proj = {}
    for p, r in zip(pos, rate):
        if p in proj:
            proj[p] += r
        else:
            proj[p] = r

    return proj


class BeamProfile:
    timestamp = None
    data = None
    proj_x = None
    proj_y = None
    u_x = None
    u_y = None
    s_x = None
    s_y = None
    norm = None

    def __init__(self, sipm_pos, scan_pos, verbose=0):
        self.sipm_pos = sipm_pos
        self.scan_pos = scan_pos
        self.verbose = verbose
        self.clear()

    def clear_fit(self):
        self.u_x = 0.
        self.u_y = 0.
        self.s_x = 0.
        self.s_y = 0.
        self.norm = 0.

    def clear(self):
        self.clear_fit()
        self.timestamp = 0.
        self.data = np.zeros(((len(self.scan_pos) * sc.n_channels), 3))
        for x_idx, x in enumerate(self.sipm_pos):
            self.data[x_idx::sc.n_channels, 0] = x
        for y_idx, y in enumerate(self.scan_pos):
            idx = y_idx * sc.n_channels
            self.data[idx:(idx + sc.n_channels), 1] = y
        self.proj_x = {}
        self.proj_y = {}

    def update_points(self, scan_idx, rates):
        idx = scan_idx * sc.n_channels
        self.data[idx:(idx + sc.n_channels), 2] = np.array(rates)
        self.proj_x = projection(self.data[:, 0], self.data[:, 2])
        self.proj_y = projection(self.data[:, 1], self.data[:, 2])
        self.timestamp = time.time()

    def save(self, filename):
        np.savetxt(filename,
                   np.append(((self.u_x, self.u_y, self.norm), (self.s_x, self.s_y, self.timestamp)), self.data,
                             axis=0),
                   delimiter=",")

    def fit(self):
        self.norm, self.u_x, self.u_y, self.s_x, self.s_y = fit.fit_2d(self.data[:, 0], self.data[:, 1],
                                                                       self.data[:, 2], verbose=self.verbose)

    def fit_fun(self):
        return fit.dist_2d(self.norm, self.u_x, self.u_y, self.s_x, self.s_y)
