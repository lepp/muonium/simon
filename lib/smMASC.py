import serial

import smConstants as sc


def mm_to_usteps(mm):
    return int(mm * sc.masc_rev_per_mm * sc.masc_eg)


def usteps_to_mm(usteps):
    return float(usteps) / float(sc.masc_eg) / float(sc.masc_rev_per_mm)


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class CommunicationError(Error):
    def __init__(self, query, reply):
        self.query = query
        self.reply = reply

    def __str__(self):
        return f'Unexpected MASC reply "{self.reply}" to query "{self.query}"'


class UnexpectedStatus(Error):
    def __init__(self, current, expected):
        self.current = current
        self.expected = expected

    def __str__(self):
        return f"MASC status is {self.current} instead of {self.expected}"


class Alarm(Error):
    def __init__(self, code):
        self.code = code

    def __str__(self):
        return f"MASC alarm code {self.code}"


class PositionMismatch(Error):
    def __init__(self, simon, masc):
        self.simon = simon
        self.masc = masc

    def __str__(self):
        return f"MASC position ({self.masc}) does not match SiMon position ({self.simon}), try reinitialising"


class ParameterMismatch(Error):
    def __init__(self, parameter, cur_value, exp_value):
        self.parameter = parameter
        self.cur_value = cur_value
        self.exp_value = exp_value

    def __str__(self):
        return f"MASC parameter {self.parameter} is {self.cur_value} instead of {self.exp_value}"


class OutOfRange(Error):
    def __init__(self, pos_mm):
        self.pos_mm = pos_mm

    def __str__(self):
        return f"Requested MASC position, {self.pos_mm}mm, out of range {sc.masc_range_mm}mm"


class Uninitialised(Error):
    def __str__(self):
        return "MASC not initialised"


class MASC:
    usteps = 0
    init = False

    def __init__(self, port, verbose=0):
        self.ser = serial.Serial(port, timeout=sc.masc_timeout_s)
        self.verbose = verbose

    def __del__(self):
        self.ser.close()

    def send_cmd(self, cmd, reply_expected, silent=False):
        verbose = self.verbose - int(silent)
        query = f"{sc.masc_rs485_adr}{cmd}\r"
        if verbose > 0:
            print(f"MASC query: {query}")
        self.ser.write(query.encode("ascii"))
        trials = 0
        reply = ""
        while trials < sc.masc_rcv_trials:
            try:
                reply += self.ser.read(1024).decode("ascii")
            except UnicodeDecodeError:
                reply = ""
            if reply:
                if reply[-1] == "\r":
                    break
            trials += 1
        if verbose > 0:
            print(f"MASC reply after {trials} trials: {reply}")
        if reply:
            if (not reply_expected) and (reply == f"{sc.masc_rs485_adr}%\r"):
                return None
            elif reply_expected and (reply[:len(query)] == f"{query[:-1]}=") and (reply[-1] == "\r"):
                return reply[len(query):-1]
        raise CommunicationError(query, reply)

    def check_init(self):
        if not self.init:
            raise Uninitialised()

    def check_status(self, code=("R",), silent=False):
        status = self.send_cmd("RS", True, silent)
        if (code is not None) and (status not in code):
            raise UnexpectedStatus(status, code)
        return status

    def check_alarm(self, code=0):
        alarm = int(self.send_cmd("AL", True))
        if alarm != code:
            raise Alarm(alarm)

    def check_parameter(self, parameter, value):
        value = str(value)
        reply = self.send_cmd(parameter, True)
        if reply != value:
            raise ParameterMismatch(parameter, reply, value)

    def set_parameter(self, parameter, value):
        self.send_cmd(f"{parameter}{value}", False)
        self.check_parameter(parameter, value)

    def check_position(self):
        masc_usteps = int(self.send_cmd("SP", True))
        if masc_usteps != self.usteps:
            raise PositionMismatch(self.usteps, masc_usteps)

    def get_position(self):
        return usteps_to_mm(self.usteps)

    def go_to(self, pos_mm, from_limit=False):
        self.check_init()
        if (pos_mm < sc.masc_range_mm[0]) or (pos_mm > sc.masc_range_mm[1]):
            raise OutOfRange(pos_mm)
        fp_status = ("FMR",)
        if from_limit:
            self.check_status(("AR",))
            self.check_alarm(sc.masc_init_limit_alarm)
            fp_status += ("AFMR",)
        else:
            self.check_status()
        self.check_position()
        self.usteps = mm_to_usteps(pos_mm)
        self.send_cmd(f"FP{self.usteps}", False)
        while self.check_status(None, True) in fp_status:
            pass
        self.check_status()
        self.check_position()

        return self.get_position()

    def go_home(self, from_limit=False):
        return self.go_to(sc.masc_home_pos_mm, from_limit)

    def jog_to_limit(self, speed):
        self.check_status()
        # Set direction and jogging speed
        self.set_parameter("DI", sc.masc_init_di)
        self.set_parameter("JS", speed)
        # Commence jogging
        self.send_cmd("CJ", False)
        while self.check_status(None, True) == "JFMR":
            pass
        # We should be in alarm now after hitting the limit switch
        self.check_status(("AR",))
        # Check that we're at the correct limit switch
        self.check_alarm(sc.masc_init_limit_alarm)

    def initialise(self):
        # If MASC is powered up after the serial port is opened, there's garbage in the buffer, so clear it first
        self.ser.reset_input_buffer()
        status = self.check_status(("R", "AR"))
        # If we're not yet a the limit switch, jog into it
        if status == "R":
            self.jog_to_limit(sc.masc_init_js_fast)
        else:
            self.check_alarm(sc.masc_init_limit_alarm)
        # Back up, and jog into the limit switch again to make the position reference as reproducible as possible
        self.send_cmd(f"FL{mm_to_usteps(sc.masc_init_backup_distance_mm)}", False)
        while self.check_status(None, True) in ("AFMR", "FMR"):
            pass
        self.jog_to_limit(sc.masc_init_js_slow)
        # Now we should have a reproducible position and are ready to set the counters accordingly
        self.usteps = mm_to_usteps(sc.masc_init_limit_pos_mm)
        self.set_parameter("SP", self.usteps)
        self.init = True

        return self.go_home(True)
