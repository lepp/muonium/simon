#!/usr/bin/env python3


import argparse
import json
import os

import numpy as np
import smConstants as sc
import smGausFit as fit
import sys

actions = "save, load, readout, kill"
default_config = "wdb_config.json"


def printe(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def save(cfg):
    printe(f"Saving WDB config to {cfg['wdb_cfg']}...")
    printe("Done.")


def load(cfg):
    printe(f"Loading WDB config from {cfg['wdb_cfg']}...")
    printe("Done.")


def readout(cfg):
    printe("Reading out WaveDREAM scalers...")
    with open(cfg["pcb_cfg"]) as pcf:
        pcb_cfg = json.load(pcf)
    with open(cfg["scan_cfg"]) as scf:
        scan_cfg = json.load(scf)
    sipm_pos = np.array([pcb_cfg["sipm_pos"][pill_chn] for pill_chn in scan_cfg["chn_map"]])
    rates = fit.dist_2d(*sc.demo_params)(sipm_pos, np.full(sipm_pos.shape, cfg["pcb_pos"])) + np.random.random(
        sipm_pos.shape) * sc.demo_params[0] * 1e-4
    # Scale to actual detector rate
    ps = pcb_cfg["pill_size"]
    rates *= ps[0] * ps[1]
    v = 100 * np.random.random(sipm_pos.shape)
    i = np.random.random(sipm_pos.shape)
    for vec in (rates, v, i):
        print(",".join([str(val) for val in vec]))
    printe("Done.")


def kill(cfg):
    printe("Killing HV...")
    printe("Done.")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("address", help="WDB address (IP or FQDN)")
    parser.add_argument("action", help=actions)
    parser.add_argument("--config", "-c", default=default_config)
    parser.add_argument("--verbose", "-v", type=int, default=0)
    parser.add_argument("--pcb_config")
    parser.add_argument("--pcb_position", type=float)
    parser.add_argument("--scan_config")
    args = parser.parse_args()

    cfg = {
        "wdb_cfg": os.path.join(sc.wdb_cfg_dir, args.config),
        "wdb_adr": args.address,
        "pcb_cfg": args.pcb_config,
        "pcb_pos": args.pcb_position,
        "scan_cfg": args.scan_config
    }

    if args.action == "save":
        save(cfg)
    elif args.action == "load":
        load(cfg)
    elif args.action == "readout":
        if (cfg["pcb_cfg"] is None) or (cfg["pcb_pos"] is None) or (cfg["scan_cfg"] is None):
            printe("ERROR: pcb_config, pcb_position, and scan_config are required for readout action!")
            sys.exit(65)
        readout(cfg)
    elif args.action == "kill":
        kill(cfg)
    else:
        printe(f"ERROR: Unknown action {args.action}! Valid choices are {actions}")
        sys.exit(64)


if __name__ == "__main__":
    main()
