#!/usr/bin/env python3


import argparse
import io
import json
import os
import pathlib
import subprocess
import tempfile

import PySimpleGUIWeb as sg
import matplotlib.collections as mcol
import matplotlib.figure as mfig
import matplotlib.pyplot as plt
import numpy as np
import serial
import smBeamProfile as bp
import smConstants as sc
import smMASC as masc
import time
from matplotlib.backends.backend_agg import FigureCanvasAgg


def sg_print(*args, **kwargs):
    print(*args, **kwargs)
    sc.win.Refresh()


def sg_error(msg):
    sg.popup_error(msg)
    sg_print()


def toggle_window_elements(disable, exclude=("-BSCNSTOP-",)):
    for element in sc.win.AllKeysDict.keys():
        if ((element[0:2] == "-B") or (element[0:3] in ("-DD", "-IN"))) and element not in exclude:
            sc.win[element].update(disabled=disable)


def masc_exception(exc):
    sg_error(f"MASC exception: {exc}")


def sg_cancelled():
    sg_print("Cancelled.")


def list_files(top_dir):
    file_list = {}
    for path, dirs, files in os.walk(top_dir):
        for file in files:
            if file[0] == ".":
                continue
            full_path = os.path.join(path, file)
            rel_path = os.path.relpath(full_path, top_dir)
            file_list[rel_path] = full_path
    return file_list


def check_file_exists(path):
    filename = os.path.basename(path)
    if pathlib.Path(path).exists():
        answer = sg.popup_yes_no(f"WARNING! {filename} already exists. Do you want to overwrite it?")
        sg_print()
        if answer == "No":
            return True
    return False


def update_file_lists(cfg):
    cfg["WDB_cfg_list"] = list_files(sc.wdb_cfg_dir)
    cfg["PCB_cfg_list"] = list_files(sc.pcb_cfg_dir)
    cfg["SCN_cfg_list"] = list_files(sc.scan_cfg_dir)
    cfg["masc_port_list"] = list_files(sc.masc_port_dir)


def update_dropdowns(cfg):
    update_file_lists(cfg)
    sc.win["-DDCFGWDB-"].update(values=cfg["WDB_cfg_list"].keys())
    sc.win["-DDCFGPCB-"].update(values=cfg["PCB_cfg_list"].keys())
    sc.win["-DDCFGSCN-"].update(values=cfg["SCN_cfg_list"].keys())
    sc.win["-DDMASCPORT-"].update(values=cfg["masc_port_list"].keys())


def run_cmd_foreground(cfg, cmd, silent):
    verbose = cfg["verbose"] - int(silent)
    if verbose > 0:
        print("Running command in foreground:")
        sg_print(cmd)
    out = subprocess.run(cmd, capture_output=True)
    if verbose > 0:
        print(f"stdout:")
        print(out.stdout.decode("utf-8"))
    if (verbose - int(silent)) >= 0:
        print(f"Done.\n")
        print(f"stderr:")
        print(out.stderr.decode("utf-8"))
        sg_print()
    if out.returncode and not silent:
        sg_error(f"{cmd[0]} returned with status {out.returncode}\nPlease check SiMon says output.")
    return out


def run_cmd_background(cfg, cmd, msg):
    if cfg["verbose"]:
        print("Running command in background:")
        sg_print(cmd)
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    sg.popup_ok(msg)
    proc.terminate()
    out, err = proc.communicate()
    print(f"Done.\n")
    print(f"stderr:")
    print(err.decode("utf-8"))
    if cfg["verbose"]:
        print(f"stdout:")
        print(out.decode("utf-8"))
    sg_print()


def check_masc(cfg):
    if "masc" in cfg:
        if cfg["masc"].init:
            return True
    sg_error("Please initialise MASC!")
    return False


def update_masc_pos_field(pos_mm):
    sc.win["-TMASCPOS-"].update(f"{pos_mm:.3f}")


def update_pcb_pos_demo(cfg, pos_mm):
    cfg["pcb_pos_demo"] = pos_mm
    update_masc_pos_field(pos_mm)


def masc_initialise(cfg, port_dd_val):
    max_time_s = float(masc.mm_to_usteps(200)) / float(sc.masc_eg) / float(sc.masc_init_js_fast) + 30
    sg_print(f"Initialising MASC. Depending on the current position, this might take up to {int(max_time_s)}s...")
    if cfg["demo"]:
        update_pcb_pos_demo(cfg, sc.masc_home_pos_mm)
        return True
    if port_dd_val is None:
        sg_error("Please select a MASC port!")
        return False
    port = cfg["masc_port_list"][port_dd_val]
    if "masc" in cfg:
        del cfg["masc"]
    try:
        cfg["masc"] = masc.MASC(port, cfg["verbose"])
        new_pos = cfg["masc"].initialise()
    except (masc.Error, serial.SerialException) as exc:
        masc_exception(exc)
        return False
    update_masc_pos_field(new_pos)
    sg_print("Done.")

    return True


def masc_gohome(cfg):
    sg_print(f"Moving PCB to home position ({sc.masc_home_pos_mm}mm)...")
    if cfg["demo"]:
        update_pcb_pos_demo(cfg, sc.masc_home_pos_mm)
        return True
    if not check_masc(cfg):
        return False
    try:
        new_pos = cfg["masc"].go_home()
    except (masc.Error, serial.SerialException) as exc:
        masc_exception(exc)
        return False
    update_masc_pos_field(new_pos)
    sg_print("Done.")

    return True


def masc_goto(cfg, pos_mm):
    sg_print(f"Moving PCB to position {pos_mm}mm...")
    if cfg["demo"]:
        update_pcb_pos_demo(cfg, pos_mm)
        return True
    if not check_masc(cfg):
        return False
    try:
        new_pos = cfg["masc"].go_to(pos_mm)
    except (masc.Error, serial.SerialException) as exc:
        masc_exception(exc)
        return False
    update_masc_pos_field(new_pos)
    sg_print("Done.")

    return True


def masc_goto_button(cfg):
    pos_text = sg.popup_get_text("Please enter desired PCB position in mm (0mm is the centre of the beam pipe)")
    if pos_text is None:
        sg_cancelled()
        return
    try:
        pos_mm = float(pos_text)
    except ValueError:
        sg_error("Invalid input")
        return
    masc_goto(cfg, pos_mm)


def check_wdb_adr(adr):
    if adr is None:
        sg_error(f"Please enter a WDB address!")
        return False
    else:
        return True


def configure_wdb(cfg, adr, cfg_dd_val):
    if not check_wdb_adr(adr):
        return
    if cfg_dd_val is None:
        sg_error("Please select a WDB config!")
        return
    wdb_cfg_file = cfg["WDB_cfg_list"][cfg_dd_val]
    sg_print(f"Configuring WDB from {wdb_cfg_file}. This will take about 30s...")
    wdb_cmd = [*cfg["wdb_cmd"], "-c", wdb_cfg_file, adr, "load"]
    run_cmd_foreground(cfg, wdb_cmd, False)


def save_wdb_config(cfg, adr):
    if not check_wdb_adr(adr):
        return
    wdb_cfg_file = sg.popup_get_text("Please enter name of new WDB config:")
    if (not wdb_cfg_file) or wdb_cfg_file is None:
        sg_cancelled()
        return
    wdb_cfg_path = os.path.join(sc.wdb_cfg_dir, wdb_cfg_file)
    if check_file_exists(wdb_cfg_path):
        sg_cancelled()
        return
    sg_print(f"Saving current WDB config to {wdb_cfg_path}...")
    wdb_cmd = [*cfg["wdb_cmd"], "-c", wdb_cfg_path, adr, "save"]
    run_cmd_foreground(cfg, wdb_cmd, False)
    update_dropdowns(cfg)


def kill_hv(cfg, adr):
    if not check_wdb_adr(adr):
        return
    sg_print("Killing HV...")
    wdb_cmd = [*cfg["wdb_cmd"], adr, "kill"]
    run_cmd_foreground(cfg, wdb_cmd, False)


def create_figure(cfg, scan_marker):
    bp = cfg["beam_profile"]
    x = bp.data[:, 0]
    y = bp.data[:, 1]
    rates = bp.data[:, 2]
    fig = mfig.Figure(figsize=sc.fig_inches, dpi=sc.fig_dpi)
    ax = fig.add_subplot(2, 2, 1)
    ax.set_aspect("equal")
    ax.set_xlim(sc.x_lim)
    ax.set_ylim(sc.y_lim)
    ax.set_xlabel(sc.x_label)
    ax.set_ylabel(sc.y_label)
    ax.grid(which="both", linestyle=":")

    ps = cfg["pcb_cfg"]["pill_size"]
    pills = [plt.Rectangle((xp, yp), ps[0], ps[1]) for xp, yp in zip((x - ps[0] / 2.), (y - ps[1] / 2.))]
    pill_collection = mcol.PatchCollection(pills, cmap=sc.pill_cmap)
    pill_collection.set_array(rates)
    col = ax.add_collection(pill_collection)

    axc = fig.add_subplot(2, 2, 4, visible=False)
    cbar = fig.colorbar(col, ax=axc, location="left")
    cbar.set_label(sc.z_label)

    if bp.norm:
        x_grid, y_grid = np.meshgrid(x, y)
        cont = ax.contour(x, y, bp.fit_fun()(x_grid, y_grid), cmap=sc.cont_cmap, linewidths=sc.cont_linewidths)
        ax.clabel(cont, cont.levels, inline=True)
        ax.set_title(
            f"norm: {bp.norm:.3f}   mean x: {bp.u_x:.3f}   mean y: {bp.u_y:.3f}   std dev x: {bp.s_x:.3f}   std dev y: {bp.s_y:.3f}"
        )

    if scan_marker is not None:
        x_offset = 2 * cfg["pcb_cfg"]["pill_size"][0]
        ax.plot((cfg["sipm_pos"][0] - x_offset), scan_marker, "w*")
        ax.plot((cfg["sipm_pos"][-1] + x_offset), scan_marker, "w*")

    axx = fig.add_subplot(2, 2, 3)
    axx.set_xlim(sc.x_lim)
    axx.set_xlabel(sc.x_label)
    axx.set_ylabel(sc.z_label)
    axx.grid(which="both", linestyle=":")
    axx.plot(list(bp.proj_x.keys()), [bp.proj_x[k] for k in bp.proj_x.keys()], "w-*")

    axy = fig.add_subplot(2, 2, 2)
    axy.set_ylim(sc.y_lim)
    axy.set_xlabel(sc.z_label)
    axy.set_ylabel(sc.y_label)
    axy.grid(which="both", linestyle=":")
    axy.plot([bp.proj_y[k] for k in bp.proj_y.keys()], list(bp.proj_y.keys()), "w-*")

    return fig


def draw_figure(fig, element):
    plt.close("all")  # erases previously drawn plots
    canv = FigureCanvasAgg(fig)
    buf = io.BytesIO()
    canv.print_figure(buf, format="png")
    if buf is None:
        return None
    buf.seek(0)
    element.update(data=buf.read())
    return canv


def update_plot(cfg, scan_marker=None):
    draw_figure(create_figure(cfg, scan_marker), sc.win["-PLOT-"])


def update_table(rates, hv, currents):
    for chn, val in enumerate(zip(rates, hv, currents)):
        sc.win[f"-TR{chn}-"].update(f"{val[0]:.3f}")
        sc.win[f"-TV{chn}-"].update(f"{val[1]:.3f}")
        sc.win[f"-TI{chn}-"].update(f"{val[2]:.3f}")


def readout_wdb(cfg, silent):
    wdb_cmd = cfg["wdb_cmd"][:]
    if cfg["demo"]:
        wdb_cmd += ["--pcb_config", cfg["pcb_cfg_file"], "--pcb_position", str(cfg["pcb_pos_demo"]), "--scan_config",
                    cfg["scan_cfg_file"]]
    wdb_cmd += [cfg["wdb_adr"], "readout"]
    out = run_cmd_foreground(cfg, wdb_cmd, silent)

    if out.returncode:
        return None

    rates, hv, currents = [np.fromstring(line, sep=",") for line in out.stdout.decode("utf-8").split("\n")[:-1]]
    update_table(rates, hv, currents)

    return rates


def scan_point(cfg, scan_pos):
    masc_status = masc_goto(cfg, scan_pos)
    if not masc_status:
        return None
    wait_s = cfg["scan_cfg"]["settling_time_s"]
    sg_print(f"Waiting {wait_s}s for scalers to stabilise...")
    time.sleep(wait_s)
    sg_print("Reading out scalers...")

    # Normalise to Hz/mm^2
    ps = cfg["pcb_cfg"]["pill_size"]
    rates = readout_wdb(cfg, False) / ps[0] / ps[1]

    return rates


def do_scan_step(cfg, val):
    def end_scan():
        sc.win["-BSCNSTOP-"].update(disabled=True)
        toggle_window_elements(False)
        cfg["scan_state"] = sc.ScanState.IDLE

    def cancel_init(error=None):
        if error is not None:
            sg_error(error)
        sg_cancelled()
        end_scan()

    if cfg["scan_state"] == sc.ScanState.IDLE:
        if val["-INWDBADR-"] and not ((val["-DDCFGPCB-"] is None) or (val["-DDCFGSCN-"] is None)):
            cfg["wdb_adr"] = val["-INWDBADR-"]
            cfg["pcb_cfg_file"] = cfg["PCB_cfg_list"][val["-DDCFGPCB-"]]
            cfg["scan_cfg_file"] = cfg["SCN_cfg_list"][val["-DDCFGSCN-"]]
            readout_wdb(cfg, True)
        return
    elif cfg["scan_state"] == sc.ScanState.START:
        sg_print("Starting new scan...")
        if not cfg["demo"]:
            if not check_masc(cfg):
                cancel_init()
                return
        if not check_wdb_adr(val["-INWDBADR-"]):
            cancel_init()
            return
        cfg["wdb_adr"] = val["-INWDBADR-"]
        if val["-DDCFGPCB-"] is None:
            cancel_init("Please select a PCB config!")
            return
        if val["-DDCFGSCN-"] is None:
            cancel_init("Please select a scan config!")
            return
        cfg["pcb_cfg_file"] = cfg["PCB_cfg_list"][val["-DDCFGPCB-"]]
        with open(cfg["pcb_cfg_file"]) as pcb_cfg:
            try:
                cfg["pcb_cfg"] = json.load(pcb_cfg)
            except json.JSONDecodeError as exc:
                cancel_init(f"Failed to parse {cfg['pcb_cfg_file']}: {exc.msg}")
                return
        cfg["scan_cfg_file"] = cfg["SCN_cfg_list"][val["-DDCFGSCN-"]]
        with open(cfg["scan_cfg_file"]) as scan_cfg:
            try:
                cfg["scan_cfg"] = json.load(scan_cfg)
            except json.JSONDecodeError as exc:
                cancel_init(f"Failed to parse {cfg['scan_cfg_file']}: {exc.msg}")
                return
        cfg["sipm_pos"] = [cfg["pcb_cfg"]["sipm_pos"][chn] for chn in cfg["scan_cfg"]["chn_map"]]
        if cfg["scan_cfg"]["scan_pos"] == "pcb":
            cfg["scan_pos"] = cfg["sipm_pos"]
        else:
            cfg["scan_pos"] = cfg["scan_cfg"]["scan_pos"]
        # Increase verbosity by 1 to always print fit parameters
        cfg["beam_profile"] = bp.BeamProfile(cfg["sipm_pos"], cfg["scan_pos"], verbose=(cfg["verbose"] + 1))
        cfg["scan_dir"] = 1
        cfg["scan_state"] += 1
        return
    elif cfg["scan_state"] > sc.ScanState.START:
        scan_pos = cfg["scan_pos"][cfg["scan_state"]]
        rates = scan_point(cfg, scan_pos)
        if rates is None:
            sg_error(f"Scan failed at position {scan_pos}! Aborting...")
            cfg["scan_state"] = sc.ScanState.STOP
            return
        cfg["beam_profile"].update_points(cfg["scan_state"], rates)
        sg_print("Success. Updating plot...")
        # Change direction at the end points and fit, except on start
        if (cfg["scan_state"] == 0) and (cfg["scan_dir"] == -1):
            fit_profile(cfg)
            cfg["scan_dir"] = 1
        elif cfg["scan_state"] == (len(cfg["scan_pos"]) - 1):
            fit_profile(cfg)
            cfg["scan_dir"] = -1
        else:
            update_plot(cfg, scan_marker=scan_pos)
        cfg["scan_state"] += cfg["scan_dir"]
        return
    elif cfg["scan_state"] == sc.ScanState.STOP:
        sg_print("Stopping scan...")
        fit_profile(cfg)
        masc_gohome(cfg)
        sg_print("\nDone.\n")
        end_scan()
        return


def fit_profile(cfg):
    if "beam_profile" not in cfg:
        return
    sg_print("Performing fit...")
    try:
        cfg["beam_profile"].fit()
        update_plot(cfg)
    except ZeroDivisionError as exc:
        sg_error(f"Fit failed with {exc}")


def download_data(cfg):
    if "beam_profile" not in cfg:
        return
    sg_print("Saving data...")
    with tempfile.TemporaryDirectory() as tmpdir:
        filename = time.strftime("simon_bp_%Y-%m-%d_%H-%M-%S_%Z.csv", time.localtime(cfg["beam_profile"].timestamp))
        path = os.path.join(tmpdir, filename)
        cfg["beam_profile"].save(path)
        file_server(cfg, path, False)
    # Redraw plot as it disappears after pop-ups
    update_plot(cfg)


def www_server():
    with open(os.path.join(sc.template_dir, "index.html"), "r") as template:
        buf = template.read()
    buf = buf.replace("%WDSURL%", sc.wds_url)
    buf = buf.replace("%FILESERVERURL%", sc.file_server_url)
    buf = buf.replace("%SIMONURL%", sc.simon_url)
    with open(os.path.join(sc.www_dir, "index.html"), "w") as index:
        index.write(buf)
    return subprocess.Popen(sc.www_cmd)


def start_wds(cfg):
    sg_print("Starting WDS...")
    wds_cmd = [sc.wds_exe, "-v", str(cfg["verbose"])]
    run_cmd_background(cfg, wds_cmd, f'WDS started. Click on "WDS" above to use. Press OK to terminate.')
    sg_print("\nStopped WDS.")


def file_server(cfg, path, upload):
    sg_print(f"Starting file server...")
    cmd = [sc.file_server_exe, "-p", str(sc.file_server_port)]
    if upload:
        cmd += ["-u"]
        msg = f'File server started. Click on "File server" above to upload your file to {path}. Press OK to terminate.'
    else:
        msg = f'File server started. Click on "File server" above to download {path}. Press OK to terminate.'
    cmd += [path]
    run_cmd_background(cfg, cmd, msg)
    if upload:
        update_dropdowns(cfg)
    sg_print("\nStopped file server.")


def delete_file(cfg, path):
    answer = sg.popup_yes_no(f"Are you sure, you want to delete {path}?")
    if (answer == "Yes") and pathlib.Path(path).exists():
        sg_print(f"Deleting {path}...")
        os.remove(path)
        update_dropdowns(cfg)
    else:
        sg_cancelled()


def process_cfg_event(cfg, event, values):
    name = event[8:11]
    if "-BCFGUPL" in event:
        if name == "WDB":
            path = sc.wdb_cfg_dir
        elif name == "PCB":
            path = sc.pcb_cfg_dir
        elif name == "SCN":
            path = sc.scan_cfg_dir
        else:
            return
        file_server(cfg, path, True)
    val = values[f"-DDCFG{name}-"]
    if val is not None:
        path = cfg[f"{name}_cfg_list"][val]
        if "-BCFGDOL" in event:
            file_server(cfg, path, False)
        elif "-BCFGDEL" in event:
            delete_file(cfg, path)


def refresh(cfg):
    update_dropdowns(cfg)
    if "beam_profile" in cfg:
        update_plot(cfg)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--demo", action="store_true")
    parser.add_argument("-v", "--verbose", action="count", default=0)
    args = parser.parse_args()

    cfg = {
        "verbose": args.verbose,
        "demo": args.demo,
        "scan_state": sc.ScanState.IDLE,
        "scan_dir": 1
    }
    cfg["wdb_cmd"] = [sc.sm_wdb_demo_exe if cfg["demo"] else sc.sm_wdb_exe]
    cfg["wdb_cmd"] += ["-v", str(cfg["verbose"])]
    cfg["pcb_pos_demo"] = 0

    update_file_lists(cfg)

    plt.style.use(sc.plt_style)
    sg.theme(sc.sg_theme)
    sg.set_options(font=sc.sg_font)
    layout = [
        [sg.B("Refresh", key="-BREFRESH-"), sg.B("Exit", key="-BEXIT-")],
        [sg.T()],
        [sg.T("Configuration", font=(sc.sg_font[0], 20))],
        [
            sg.Column([
                [sg.T("MASC port")],
                [sg.T("WDB address")],
                [sg.T("WDB config")],
                [sg.T("PCB config")],
                [sg.T("Scan config")]
            ]),
            sg.Column([
                [sg.DropDown(cfg["masc_port_list"].keys(), key="-DDMASCPORT-", size=(50, 1)),
                 sg.B("Initialise", key="-BMASCINIT-"), sg.B("Go home", key="-BMASCGOHOME-"),
                 sg.B("Go to", key="-BMASCGOTO-")],
                [sg.In(key="-INWDBADR-", size=(50, 1))],
                [sg.DropDown(cfg["WDB_cfg_list"].keys(), key="-DDCFGWDB-", size=(50, 1)),
                 sg.B("Download", key="-BCFGDOLWDB-"), sg.B("Delete", key="-BCFGDELWDB-", button_color="red"),
                 sg.B("Upload", key="-BCFGUPLWDB-"), sg.B("Recall", key="-BWDBREC-"), sg.B("Save", key="-BWDBSAV-"),
                 sg.B("Manual configuration", key="-BWDSSTART-"),
                 sg.B("Kill HV", key="-BWDBKILL-", button_color="red")],
                [sg.DropDown(cfg["PCB_cfg_list"].keys(), key="-DDCFGPCB-", size=(50, 1)),
                 sg.B("Download", key="-BCFGDOLPCB-"), sg.B("Delete", key="-BCFGDELPCB-", button_color="red"),
                 sg.B("Upload", key="-BCFGUPLPCB-")],
                [sg.DropDown(cfg["SCN_cfg_list"].keys(), key="-DDCFGSCN-", size=(50, 1)),
                 sg.B("Download", key="-BCFGDOLSCN-"), sg.B("Delete", key="-BCFGDELSCN-", button_color="red"),
                 sg.B("Upload", key="-BCFGUPLSCN-")]
            ]),
        ],
        [sg.T()],
        [sg.T("PCB position"), sg.T("0", key="-TMASCPOS-", size=(10, 1), justification="r"), sg.T("mm")],
        [sg.T()],
        [
            sg.Column([
                [sg.T("WDB channel", size=(10, 1))],
                [sg.T("Rate [Hz]", size=(10, 1))],
                [sg.T("HV [V]", size=(10, 1))],
                [sg.T("I [uA]", size=(10, 1))]
            ])

        ] + [
            sg.Column([
                [sg.T(f"{i}", size=(10, 1), justification="r")],
                [sg.T("0", key=f"-TR{i}-", size=(10, 1), justification="r")],
                [sg.T("0", key=f"-TV{i}-", size=(10, 1), justification="r")],
                [sg.T("0", key=f"-TI{i}-", size=(10, 1), justification="r")]
            ]) for i in range(sc.n_channels)
        ],
        [sg.T()],
        [sg.T("SiMon says", font=(sc.sg_font[0], 20))],
        [sg.Output(size=(100, 10))],
        [sg.T()],
        [sg.T("Scan", font=(sc.sg_font[0], 20))],
        [sg.B("Run", key="-BSCNRUN-"), sg.B("Stop", key="-BSCNSTOP-", button_color="red", disabled=True),
         sg.B("Download data", key="-BBPDOL-", disabled=True)],
        [sg.Image(key="-PLOT-")]
    ]

    sc.win = sg.Window("SiMon", layout, web_port=sc.simon_port, web_start_browser=False)
    index_httpd = www_server()
    print(f"Started web server. Now point your browser to {sc.www_url}")
    # Need to read window first, otherwise the update causes a crash.
    # From now on stdout will be redirected to the Output element in the window
    sc.win.read(sc.win_timeout_ms)
    sc.win["-INWDBADR-"].update("demo" if cfg["demo"] else sc.default_wdb_adr)

    last_event = None
    while True:
        try:
            event, values = sc.win.read(sc.win_timeout_ms)
            do_scan_step(cfg, values)
            if (cfg["verbose"] and (event != "__TIMEOUT__")) or (cfg["verbose"] > 2):
                sg_print(f"Processing event {event}...")
            if event != last_event:
                last_event = event
                if event == "-BWDBKILL-":
                    kill_hv(cfg, values["-INWDBADR-"])
                elif event in ("-BEXIT-", sg.WIN_CLOSED):
                    break
                elif event == "-BREFRESH-":
                    toggle_window_elements(True)
                    refresh(cfg)
                    toggle_window_elements(False)
                elif "-BCFG" in event:
                    toggle_window_elements(True)
                    process_cfg_event(cfg, event, values)
                    toggle_window_elements(False)
                elif event == "-BMASCINIT-":
                    toggle_window_elements(True)
                    masc_initialise(cfg, values["-DDMASCPORT-"])
                    toggle_window_elements(False)
                elif event == "-BMASCGOHOME-":
                    toggle_window_elements(True)
                    masc_gohome(cfg)
                    toggle_window_elements(False)
                elif event == "-BMASCGOTO-":
                    toggle_window_elements(True)
                    masc_goto_button(cfg)
                    toggle_window_elements(False)
                elif event == "-BWDBSAV-":
                    toggle_window_elements(True)
                    save_wdb_config(cfg, values["-INWDBADR-"])
                    toggle_window_elements(False)
                elif event == "-BWDBREC-":
                    toggle_window_elements(True)
                    configure_wdb(cfg, values["-INWDBADR-"], values["-DDCFGWDB-"])
                    toggle_window_elements(False)
                elif event == "-BWDSSTART-":
                    toggle_window_elements(True)
                    start_wds(cfg)
                    toggle_window_elements(False)
                elif event == "-BSCNRUN-":
                    if cfg["scan_state"] == sc.ScanState.IDLE:
                        toggle_window_elements(True, ("-BSCNSTOP-", "-BWDBKILL-"))
                        sc.win["-BSCNSTOP-"].update(disabled=False)
                        cfg["scan_state"] = sc.ScanState.START
                elif event == "-BSCNSTOP-":
                    if cfg["scan_state"] > sc.ScanState.START:
                        sc.win["-BSCNSTOP-"].update(disabled=True)
                        cfg["scan_state"] = sc.ScanState.STOP
                elif event == "-BBPDOL-":
                    toggle_window_elements(True)
                    download_data(cfg)
                    toggle_window_elements(False)
        except KeyboardInterrupt:
            break
    sc.win.close()
    index_httpd.terminate()


if __name__ == "__main__":
    main()
