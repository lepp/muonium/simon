#include <cstdio>
#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <string>
#include <vector>

#include <argp.h>

#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/filewritestream.h"
#include "rapidjson/prettywriter.h"

#include "git-revision.h"
#include "WDBLib.h"


#define N_CHANNELS 16
#define DEFAULT_CONFIG "wdb_config.json"
#define ACTIONS "save, load, readout, kill"
#define TRIG_OUT 1


const char *argp_program_version = GIT_REVISION;
const char *argp_program_bug_address = "<dg.code+simon@protonmail.ch>";

// Program documentation
static char doc[] = "SiMon -- DAQ for the SiPM-based beam Monitor (SiMon)";

// Description of positional arguments
static char args_doc[] = "ADDRESS ACTION";

// Optional arguments
static struct argp_option options[] = {
        {"config",        'c', "CONFIG", 0, "WDB config file"},
        {"verbosity",     'v', "VERB",   0, "Verbosity level"},
        {"wdb_verbosity", 'V', "VERB",   0, "WDB verbosity level. Use with caution, as messages on stdout will confuse SiMon."},
        {nullptr}
};

// Used by main to communicate with parse_opt
struct arguments {
    int verbosity = 0;
    int wdbVerbosity = 0;
    std::string config;
    std::vector<std::string> pos;
};

// Argp parser function
static error_t parse_opt(int key, char *arg, struct argp_state *state) {
    // Get the input argument from argp_parse, which we know is a pointer to our arguments structure
    auto *args = static_cast<arguments *>(state->input);
    switch (key) {
        case 'c':
            args->config = arg;
            break;
        case 'v':
            args->verbosity = std::stoi(arg);
            break;
        case 'V':
            args->wdbVerbosity = std::stoi(arg);
            break;
        case ARGP_KEY_ARG:
            if (state->arg_num >= 2) {
                // Too many arguments
                argp_usage(state);
            }
            args->pos.emplace_back(arg);
            break;
        case ARGP_KEY_END:
            if (state->arg_num < 2) {
                // Not enough arguments
                argp_usage(state);
            }
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

// Argp parser
static struct argp parser = {options, parse_opt, args_doc, doc};


/////////////
// Globals //
/////////////

typedef struct GLOBALS {
    std::string address;
    std::string action;
    std::string config;
    int verbose = 0;
    int wdbVerbose = 0;
} GLOBALS;


//////////////////
// JSON parsing //
//////////////////

void parseError(const std::string &entry, const std::string &fileName, const int idx = -1) {
    if (idx == -1) {
        throw std::runtime_error("Failed to parse " + entry + " from " + fileName);
    } else {
        std::cerr << "WARNING: Failed to parse value number " << idx << " of array " << entry << " from " << fileName
                  << "! Skipping..." << std::endl;
    }
}

rapidjson::Document readJson(const std::string &fileName) {
    FILE *jsonFile = std::fopen(fileName.c_str(), "r");
    if (!jsonFile) {
        throw std::runtime_error("Failed to open " + fileName);
    }
    char jsonBuffer[65536];
    rapidjson::FileReadStream jsonStream(jsonFile, jsonBuffer, sizeof(jsonBuffer));
    rapidjson::Document jsonDoc;
    jsonDoc.ParseStream(jsonStream);
    std::fclose(jsonFile);
    if (jsonDoc.HasParseError() || !jsonDoc.IsObject()) {
        throw std::runtime_error("Failed to parse " + fileName);
    }
    return jsonDoc;
}

void writeJson(const rapidjson::Document &jsonDoc, const std::string &fileName) {
    FILE *jsonFile = std::fopen(fileName.c_str(), "w");
    if (!jsonFile) {
        throw std::runtime_error("Failed to open " + fileName);
    }
    char jsonBuffer[65536];
    rapidjson::FileWriteStream jsonStream(jsonFile, jsonBuffer, sizeof(jsonBuffer));
    rapidjson::PrettyWriter<rapidjson::FileWriteStream> jsonWriter(jsonStream);
    jsonDoc.Accept(jsonWriter);
    std::fclose(jsonFile);
}


///////////////////////////
// WDB utility functions //
///////////////////////////

void rebootWDB(const GLOBALS &gl, WDB *b) {
    if (gl.verbose) {
        std::cerr << "Reboot " << b->GetAddr() << std::endl;
    }
    b->ReconfigureFpga();
    sleep_ms(5000);
    if (gl.verbose) {
        std::cerr << "Finished" << std::endl;
    }
}

WDB *connectWDB(const GLOBALS &gl, const bool reboot = false) {
    WDB *b = new WDB(gl.address, gl.wdbVerbose);
    b->SetVerbose(gl.wdbVerbose);
    b->Connect();
    if (reboot) {
        rebootWDB(gl, b);
        b->Connect();
    }

    for (int i = 0; i < 50; ++i) {
        b->ReceiveStatusRegisters();
        int s = b->GetBoardMagic();
        if (s != 0xAC) {
            sleep_ms(100);
            std::cerr << "Wait for board magic number" << std::endl;
        } else {
            break;
        }
        if (i == 49) {
            throw std::runtime_error(std::string("Error reading magic number from " + b->GetName()));
        }
    }

    b->ReceiveControlRegisters();
    if (gl.wdbVerbose) {
        std::cout << "\n========== WDB Info ==========" << std::endl;
        b->PrintVersion();
    }

    return b;
}

void disconnectWDB(const GLOBALS &gl, WDB *b) {
    if (gl.verbose) {
        std::cerr << "Disconnect from " << b->GetAddr() << std::endl;
    }
    delete b;
}

void killHV(const GLOBALS &gl, WDB *b) {
    std::cerr << "Turn off HV" << std::endl;
    for (int chn = 0; chn < N_CHANNELS; ++chn) {
        b->SetHVTarget(chn, 0.);
    }
}

std::vector<float> getHV(const GLOBALS &gl, WDB *b) {
    std::vector<float> v;
    b->GetHVTarget(v);

    return v;
}

std::vector<float> getCurrents(const GLOBALS &gl, WDB *b) {
    std::vector<float> i;
    b->GetHVCurrents(i, true);

    return i;
}

std::vector<uint64_t> getScalers(const GLOBALS &gl, WDB *b) {
    std::vector<uint64_t> scaler;
    b->GetScalers(scaler, true);

    return scaler;
}

void loadConfig(const GLOBALS &gl, WDB *b) {
    rapidjson::Document config = readJson(gl.config);
    std::string entry = "HVTarget";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsArray()) {
        int chn = 0;
        for (const auto &value : config[entry.c_str()].GetArray()) {
            if (value.IsFloat()) {
                b->SetHVTarget(chn, value.GetFloat());
            } else {
                parseError(entry, gl.config, chn);
            }
            ++chn;
        }
        std::cerr << "Setting " << entry << " to:\n";
        for (const auto &v : getHV(gl, b)) {
            std::cerr << v << ",";
        }
        std::cerr << "\b " << std::endl;
        const unsigned waits = 20;
        std::cerr << "Waiting " << waits << "s for currents [uA] to stabilise..." << std::endl;
        for (unsigned t = 0; t < waits; ++t) {
            sleep_ms(1000);
            for (const auto &i : getCurrents(gl, b)) {
                std::cerr << i << ",";
            }
            std::cerr << "\b " << std::endl;
        }
        std::cerr << "Set " << entry << " to:\n";
        for (const auto &v : getHV(gl, b)) {
            std::cerr << v << ",";
        }
        std::cerr << "\b " << std::endl;
    } else {
        parseError(entry, gl.config);
    }

    entry = "FeGain";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsArray()) {
        int chn = 0;
        for (const auto &value : config[entry.c_str()].GetArray()) {
            if (value.IsFloat()) {
                b->SetFeGain(chn, value.GetFloat());
                if (gl.verbose) {
                    std::cerr << "Set " << entry << " for channel " << chn << " to " << b->GetFeGain(chn) << std::endl;
                }
            } else {
                parseError(entry, gl.config, chn);
            }
            ++chn;
        }
    } else {
        parseError(entry, gl.config);
    }

    entry = "Range";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsFloat()) {
        b->SetRange(config[entry.c_str()].GetFloat());
        if (gl.verbose) {
            std::cerr << "Set " << entry << " to " << b->GetRange() << std::endl;
        }
    } else {
        parseError(entry, gl.config);
    }

    entry = "DacTriggerLevelV";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsArray()) {
        int chn = 0;
        for (const auto &value : config[entry.c_str()].GetArray()) {
            if (value.IsFloat()) {
                b->SetDacTriggerLevelV(chn, value.GetFloat());
                if (gl.verbose) {
                    std::cerr << "Set " << entry << " for channel " << chn << " to " << b->GetDacTriggerLevelV(chn)
                              << std::endl;
                }
            } else {
                parseError(entry, gl.config, chn);
            }
            ++chn;
        }
    } else {
        parseError(entry, gl.config);
    }

    // Hard-code this for now, as it's not yet implemented in WDS
    entry = "ExtTriggerOutEnable";
    b->SetExtTriggerOutEnable(TRIG_OUT);
    if (gl.verbose) {
        std::cerr << "Set " << entry << " to " << b->GetExtTriggerOutEnable() << std::endl;
    }
}

void saveConfig(const GLOBALS &gl, WDB *b) {
    rapidjson::Document config;
    config.SetObject();
    rapidjson::Document::AllocatorType &allocator = config.GetAllocator();
    rapidjson::Value val;

    val.SetArray();
    for (const auto &v : getHV(gl, b)) {
        val.PushBack(v, allocator);
    }
    config.AddMember("HVTarget", val, allocator);

    val.SetArray();
    for (int chn = 0; chn < N_CHANNELS; ++chn) {
        val.PushBack(b->GetFeGain(chn), allocator);
    }
    config.AddMember("FeGain", val, allocator);

    config.AddMember("Range", b->GetRange(), allocator);

    val.SetArray();
    for (int chn = 0; chn < N_CHANNELS; ++chn) {
        val.PushBack(b->GetDacTriggerLevelV(chn), allocator);
    }
    config.AddMember("DacTriggerLevelV", val, allocator);

    writeJson(config, gl.config);
}

template<typename T>
void printChannelVector(T vec) {
    for (std::size_t chn = 0; chn < N_CHANNELS; ++chn) {
        std::cout << vec[chn] << ((chn < (N_CHANNELS - 1)) ? "," : "\n");
    }
    std::cout << std::flush;
}


/////////////
// Actions //
/////////////

void load(const GLOBALS &gl) {
    WDB *b = connectWDB(gl, true);
    loadConfig(gl, b);
    disconnectWDB(gl, b);
}

void save(const GLOBALS &gl) {
    WDB *b = connectWDB(gl);
    saveConfig(gl, b);
    disconnectWDB(gl, b);
}

void readout(const GLOBALS &gl) {
    WDB *b = connectWDB(gl);
    printChannelVector(getScalers(gl, b));
    printChannelVector(getHV(gl, b));
    printChannelVector(getCurrents(gl, b));
    disconnectWDB(gl, b);
}

void kill(const GLOBALS &gl) {
    WDB *b = connectWDB(gl);
    killHV(gl, b);
    disconnectWDB(gl, b);
}


int main(int argc, char *argv[]) {
    struct arguments args;
    argp_parse(&parser, argc, argv, 0, nullptr, &args);

    GLOBALS gl;
    gl.address = args.pos[0];
    gl.action = args.pos[1];
    gl.config = args.config.empty() ? (std::string(std::getenv("SIMONSYS")) + "/config/wdb/" + DEFAULT_CONFIG)
                                    : args.config;
    gl.verbose = args.verbosity;
    gl.wdbVerbose = args.wdbVerbosity;

    switch (gl.action.c_str()[0]) {
        case 's':
            save(gl);
            break;
        case 'l':
            load(gl);
            break;
        case 'r':
            readout(gl);
            break;
        case 'k':
            kill(gl);
            break;
        default:
            std::cerr << "ERROR: Unknown action " << gl.action << "! Valid choices are " << ACTIONS << std::endl;
            exit(64);

    }

    return 0;
}
