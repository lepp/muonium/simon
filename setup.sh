SIMONSYS="$(readlink -e "$(dirname "${BASH_SOURCE[0]}")")"
WDBSYS="${SIMONSYS}"
PATH="${SIMONSYS}/bin:${WDBSYS}/sw/bin:${SIMONSYS}/servefile/servefile:${PATH}"
PYTHONPATH="${SIMONSYS}/lib:${SIMONSYS}/servefile/servefile:${PYTHONPATH}"

export SIMONSYS WDBSYS PATH PYTHONPATH
