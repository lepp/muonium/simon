#!/bin/bash

script_name="start_simon.sh"
simon_path="$(readlink -e "$(dirname "${BASH_SOURCE[0]}")/..")"
script_path="${simon_path}/${script_name}"
conda_path="$(readlink -e "${CONDA_PREFIX}/../..")"

cat <<EOF >"${script_path}"
#!/bin/bash

# conda from script hack from https://github.com/conda/conda/issues/7980
. "${conda_path}/etc/profile.d/conda.sh"
conda activate "${CONDA_DEFAULT_ENV}"
. "${simon_path}/setup.sh"
SiMon.py "\${@}"
EOF

chmod +x "${script_path}"
