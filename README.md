# SiMon

DAQ software for the SiPM-based beam Monitor. SiMon is completely web-based, and thus doesn't require graphical remote
access to the DAQ machine.

## Contents

[Requirements](#requirements)

[Installation](#installation)

[Electrical setup](#electrical-setup)

[Quick start](#quick-start)

[Usage](#usage)

[Data formats](#data-formats)

[Running SiMon behind a firewall](#running-simon-behind-a-firewall)

## Requirements

SiMon has been tested on GNU/Linux. The following Python packages are required:

- Python 3
- NumPy
- Matplotlib
- pySerial
- PySimpleGUIWeb

A clean and easy way to install these is to install [Miniconda](https://docs.conda.io/en/latest/miniconda.html)
(or [Anaconda](https://anaconda.com)) and then create a new environment with the following command:

```
$ conda create -n simon python numpy matplotlib pyserial pysimpleguiweb
```

Replace `simon` with a name of your liking.

In addition, a working C++ toolchain is required. On Debian-based GNU/Linux distributions this can usually be achieved
by installing `build-essential`:

```
# apt install build-essential
```

## Installation

1. Clone this repository. As it contains Git submodules, you'll have to clone it recursively:

```
$ git clone --recurse-submodules https://gitlab.ethz.ch/lepp/muonium/simon.git
```

2. Source the required environment. If you've followed the steps in [Requirements](#requirements):

```
$ conda activate simon
```

3. Source the SiMon environment:

```
$ . /path/to/simon/setup.sh
```

4. (optional) If you're using the Anaconda Python enviroment from [Requirements](#requirements), you can create a start
   script for SiMon, that will source all requirements automatically:

```
$ /path/to/simon/templates/create_start_script.sh
```

This script is location independent, and can be moved to an arbitraty place. However, it'll need to be recreated if the
location of SiMon or any of the dependencies changes.

5. Build SiMon:

```
$ cd /path/to/simon
$ mkdir build
$ cd build
$ cmake ..
$ cmake --build . --target install -- -j $(nproc)
```

Note that `--target install` is required in order to move the binaries to the expected location. The latter is under the
repository folder, hence no superuser privileges are required.

6. Build WDS:

```
$ cd /path/to/simon/sw
$ mkdir build
$ cd build
$ cmake ..
$ cmake --build . --target install -- -j $(nproc)
```

Note that `--target install` is required in order to move the binaries to the expected location. The latter is under the
repository folder, hence no superuser privileges are required.

## Electrical setup

**WARNING: NEVER (DIS)CONNECT MOTOR WHILE CONTROLLER IS SWITCHED ON! ALWAYS TURN OFF CONTROLLER FIRST AND WAIT ONE
MINUTE!**

1. The WaveDREAM can only read out 16 sensors at a time. Hence you need to choose which sensors you want to use for the
   scan. Connect the corresponding channels from the feedthrough to the WaveDREAM inputs. Make sure to properly reflect
   this in the channel map in the scan configuration (see [Data formats](#data-formats)).

2. Connect WaveDREAM and DAQ computer to the same **GIGABIT** ethernet switch, which in turn connects to the PSI
   network. If you encounter connection problems, use a dedicated gigabit switch without power over Ethernet (PoE).

3. Connect motor and limit switches (two separate cables) to motor controller (MASC).

4. Connect MASC to DAQ computer via USB RS-485 cable, **TERMINATE** other RS-485 socket on controller. The terminator
   looks like a connector without a cable, ideally it's always left connected.

5. If you see excessive noise on the WaveDREAM channels, make sure to ground the WaveDREAM properly to SiMon, ideally
   using one of the front panel screws. Furthermore, the stepper motor has been found to be noisy sometimes. This could
   be mitigated in the past by shorting the casing of the motor connector to SiMon and/or the WaveDREAM.

## Quick start

1. Start SiMon with the script created above:

```
$ ./start_simon.sh
```

2. Point your browser to the address indicated in the terminal.

3. Select proper MASC port, WDB address, and config files.

4. Press `Run`

## Usage

If you've created a start script, it's enough to run that:

```
$ ./start_simon.sh
```

Without start script you'll have to source the environment manually. For instance:

```
$ conda activate simon
$ . /path/to/simon/setup.sh
$ SiMon.py
```

Note that the full path needs only be specified for SiMon's setup script. This will add the path to the SiMon
executables to your `PATH`.

SiMon is mostly web-based, and thus has only two command line arguments:
`-d` switches on demo mode. This can be used to test SiMon without the actual hardware connected.
`-v` increases verbosity. It can be used multiple times for different levels. More than one occurrence is only useful
for debugging, as it will clutter the terminal and impair responsiveness.

After starting SiMon, it needs to be accessed from a browser via the address provided in the terminal output. This is
possible from any machine that can directly connect to the machine SiMon is running on.

An overview of the controls of SiMon's web interface is shown below

![Controls](doc/README/controls.png)

1. Links to the WaveDREAM (`WDS`) and file servers. These are used to manually configure the WaveDREAM board, and
   up/download configuration files. They first need to be started (see 5).
2. The refresh button can be used to force a rebuild of the page when something is wrong (e.g. missing plots or dropdown
   entries). This is different from simply refreshing the page in the browser. Exit quits SiMon. It will need to be
   restarted as described above.
3. Serial port of the stepper motor (MASC). If the standard cable is used, this will be a USB-to-RS485 adaptor. Ignored
   in demo mode. After a fresh start and whenever the connection to the MASC controller is lost, the position reference
   needs to be reinitialised. The other two buttons allow a manual movement of the PCB to home or an arbitrary position.
4. Address of the WaveDREAM board. Usually `wdXXX.domain.tld` where `XXX` is a number. An IP address will work too.
5. WDB, PCB, and scan config files. For all of these, the dropdown list will show all available files. New ones can be
   added via the file server (see 1) after pressing `Upload`. Existing ones can be deleted or downloaded. PCB and scan
   configs are applied at the scan start. Reconfiguring the WDB takes around 30s, and is only necessary when the
   configuration is changed. Therefore, this needs to be done manually by pressing the `Recall` button. This will load
   the config from the selected file, and apply it to the WDB. Manually editing the WDB config can be quite cumbersome.
   Therefore, this can be done using the WDB live oscilloscope called WaveDREAM server (WDS). It can be started using
   the `Manual configuration` button and using the link under 1. After stopping the WDS, the configuration can be saved
   to a file using the `Save` button, after which it will appear in the dropdown list. The HV can be killed at any time,
   except during motor movement. It can be reenabled either manually or by recalling an existing configuration.
6. Current PCB position. This is the y-axis in the plots. 0mm is the centre of the beam pipe.
7. Effective rates, voltages and currents of all WDB channels. Random values are generated in demo mode.
8. Terminal output of SiMon. Verbosity can be increased by starting SiMon with the `-v` parameter (see above).
9. After pressing `Run`, SiMon will continously move back and forth, and scan all the points given in the scan
   configuration (in both directions). Gaussian fits are calculated at both travel end points, and upon stopping. When
   stopped, SiMon will move back to its home position. The data can be downloaded as an ASCII file via the file server (
   1).

Below is an example plot set (created using demo mode).

![Plots](doc/README/plots.png)

The top left shows the beam profile, top right and bottom left show the projections (sums) on the y- and x-axis,
respectively. x corresponds to the positions of the SiPMs on the PCB, while y represents the position of the sensor row
in perpendicular direction. The origin is at the centre of the beam pipe. Finally, the bottom right shows the colour
scale of the profile plot, representing the event rate at each position. The profile is fitted with a 2D gaussian with
the fit values displayed on top of the profile plot. The norm is the integral of the gaussian. For a successful fit,
this should correspond to the total beam current, unless the beam ellipse is neither parallel to the x- nor the y-axis.
The two white stars in the plot (not shown in stopped state as above) indicate the current scanning position. The plots
can be downloaded by right-clicking on them and then `Save image` or similar.

## Data formats

All config files are formatted with [JSON](https://json.org).

### WDB config

These files are used to save and recall WDB configurations. Generation is easiest using the WDB server. Currently only
HV, front-end gain, ADC range, and trigger levels are saved. I.e., when something else is needed, it needs to be
manually set using the WDS. On the other hand, it's usually a good idea to recall the configuration from file after
using the WDS to make sure no other settings were changed. Note that in addition, the external trigger output is always
enabled when recalling a configuration from file (this cannot be set from the WDS currently).

### PCB config

This contains the PCB geometry, i.e. the sensor positions in mm (along the PCB with 0mm in the centre), and the
scintillator pill size in mm. Creating a new PCB config is only necessary when using a new PCB geometry. Otherwise, it's
enough to select the proper one.

### Scan config

All scan-related options are set here. This is the file that needs to be adapted most often in practice. The WDB has
only 16 channels while the current SiMon feedthrough and cabling allow for up to 20 sensors on the PCB. Hence, one needs
to choose 16 sensors out of the 20 to use for a scan. To select the correct geometrical positions for the plots, the
scan configuration contains a channel map, which is an array of 16 integers, which correspond to the 16 connected
sensors (starting from 0). Another array contains an arbitrary number of scan positions with 0mm at the centre of the
beam pipe. Instead of an array of floats, this can also just contain the string `pcb`, in which case, the positions of
the active PCB sensors are used, such that the scan grid will become rectangular. Finally, this file also contains a
parameter called settling time, which tells SiMon how long it should wait at a position before recording the rate. While
shorter times will increase scanning speed, for production this should not be set below 2s as lower values will result
in a skew of the data in scanning direction because the settling time is too short for the WDB integrators.

### Scan data

The data from the current scan can be downloaded by clicking on `Download data` after stopping the scan. The file format
is a three-column CSV file. The columns correspond to the position in x and y in mm, and the rate in Hz/mm^2. Hence,
each row contains the rate of a single sensor at a certain position. The first two rows are special. They contain the
fit values, and a Unix timestamp. The order (top left to bottom right) is the following: mean x, mean y, norm, sigma x,
sigma y, timestamp.

## Running SiMon behind a firewall

SiMon implements no access control or encryption whatsoever. Therefore, it's strongly discouraged to expose SiMon to the
internet. Remote access can be achieved using a VPN or SSH. Currently, SiMon runs on several ports, and uses hard-coded
paths to the fully-qualified domain name of the machine it is running on. Therefore, it's not practical to use SSH's TCP
port forwarding (`-L port:host:hostport`). Instead, SSH's dynamic application-level port forwarding (`-D proxyport`)
should be used. A browser can then be instructed to use `socks5://localhost:proxyport` as its proxy server.
